
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Renata
 */
public class IrrigationSystemSensor {
    
    private int humidityValue; 
    private String location; 
    
    IrrigationSystemSensor(int value, String location){
        this.humidityValue = value;
        this.location = location;
    }
         
    void display(){
        System.out.println("Sensor value at location "+this.location+" is: "+this.humidityValue);
    }
    
    void setHumidityValue(int value){
       humidityValue=value;
    }
    
    int getHumidityValue(){
        return humidityValue;    
    }
    
    
    public static void main(String[] args) throws InterruptedException {
        Random random = new Random();
        
        /*for(int i=0; i<10; i++)
        {
            System.out.println(random.nextInt(2));
        }
        */
        // random.nextDouble();
       IrrigationSystemSensor sensor1 = new IrrigationSystemSensor(random.nextInt(2),"A");
        IrrigationSystemSensor sensor2 = new IrrigationSystemSensor(random.nextInt(2), "B");
        IrrigationSystemSensor sensor3 =new IrrigationSystemSensor(random.nextInt(2), "C");
        IrrigationSystemSensor sensor4 =new IrrigationSystemSensor(random.nextInt(2), "D");
        IrrigationSystemSensor sensor5 =new IrrigationSystemSensor(random.nextInt(2), "E");
        Controller c1=new Controller(sensor1);
        Controller c2=new Controller(sensor2);
        Controller c3=new Controller(sensor3);
        Controller c4=new Controller(sensor4);
        Controller c5=new Controller(sensor5);
        
        
      while(true){
          
        System.out.println("The value for sensor 1 is: " +sensor1.getHumidityValue());
        System.out.println("The value for sensor 2 is: " +sensor2.getHumidityValue());
        System.out.println("The value for sensor 3 is: " +sensor3.getHumidityValue());
        System.out.println("The value for sensor 4 is: " +sensor4.getHumidityValue());
        System.out.println("The value for sensor 5 is: " +sensor5.getHumidityValue());
       
        sensor1.setHumidityValue(random.nextInt(2));
        sensor1.display();
        c1.controlStep();
        sensor2.setHumidityValue(random.nextInt(2));
        sensor2.display();
        c2.controlStep();
        sensor3.setHumidityValue(random.nextInt(2));
        sensor3.display();
        c3.controlStep();
        sensor4.setHumidityValue(random.nextInt(2));
        sensor4.display();
        c4.controlStep();
        sensor5.setHumidityValue(random.nextInt(2));
        sensor5.display();
        c5.controlStep();
       
        Thread.sleep(1000);
      }
    }
}

