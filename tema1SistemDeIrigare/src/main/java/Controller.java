/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Renata
 */
public class Controller {
    
    private IrrigationSystemSensor sensorHumidity;

    public Controller(IrrigationSystemSensor sensorHumidityObj) {
        this.sensorHumidity = sensorHumidityObj;
    }
    
    void controlStep(){
        if(sensorHumidity.getHumidityValue()==1)
            System.out.println("Stop irrigating!");
        else if(sensorHumidity.getHumidityValue()==0)
            System.out.println("Start irrigating!");
        else 
            System.out.println("Incorrect sensor value, check sensor! "+sensorHumidity.getHumidityValue());
    }
}
    

