package lab2.g1;

public class Robot {

    private int x;
    private int y;
    private String id;

    public Robot(String id, int x, int y) {
        this.x = x;
        this.y = y;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void move(String dir) {
        switch (dir) {
            case "right":
                if (x < 4) {
                    x = x + 1;
                } else {
                    System.out.println("Can't move right, the edge of the board has been reached");
                }
                break;
            case "left":
                if (x > 0) {
                    x = x - 1;
                } else {
                    System.out.println("Can't move left, the edge of the board has been reached");
                }
                break;
            case "up":
                if (y > 0) {
                    y = y - 1;
                } else {
                    System.out.println("Can't move up, the edge of the board has been reached");
                }
                break;
            case "down":
                if (y < 4) {
                    y = y + 1;
                } else {
                    System.out.println("Can't move down, the edge of the board has been reached");
                }
                break;

        }
    }
}
