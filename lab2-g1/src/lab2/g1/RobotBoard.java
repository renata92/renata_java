package lab2.g1;

import java.util.Random;

public class RobotBoard {

    private Robot[][] matrix = new Robot[5][5];

    void addRobot(Robot r) {
        matrix[r.getX()][r.getY()] = r;
    }

    void randomMove(Robot r) {
        //sa se deplaseze cu o pozitie generata random fiecare robot
        //daca un robot ajunge pe o pozitie ocupata de lat robot ambii roboti sunt distrusi (stersi de pe board)
        Random random = new Random();
        int directionNr = random.nextInt(4);

        matrix[r.getX()][r.getY()] = null;

        switch (directionNr) {
            case 0:
                r.move("up");
                if (matrix[r.getX()][r.getY()] != null) {
                    System.out.print("Boom!");
                    System.exit(0);
                } else {
                    matrix[r.getX()][r.getY()] = r;
                }
                break;
            case 1:
                r.move("right");
                if (matrix[r.getX()][r.getY()] != null) {
                    System.out.print("Boom!");
                    System.exit(0);
                } else {
                    matrix[r.getX()][r.getY()] = r;
                }
                break;
            case 2:
                r.move("down");
                if (matrix[r.getX()][r.getY()] != null) {
                    System.out.print("Boom!");
                    System.exit(0);
                } else {
                    matrix[r.getX()][r.getY()] = r;
                }
                break;
            case 3:
                r.move("left");
                if (matrix[r.getX()][r.getY()] != null) {
                    System.out.print("Boom!");
                    System.exit(0);
                } else {
                    matrix[r.getX()][r.getY()] = r;
                }
                break;
            default:
                System.out.println("Error occurred! " + directionNr);
                break;
        }

    }

    void display() {
        System.out.println("Diplsay robot board:");
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] != null) {
                    System.out.print(matrix[i][j].getId() + " ");
                } else {
                    System.out.print("X ");
                }
            }
            System.out.println("");
        }
    }
}
