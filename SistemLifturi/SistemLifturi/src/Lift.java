import static java.lang.Thread.sleep;

public class Lift extends BazaLift {
    public Lift(int pozitieEtaj, boolean inMiscare, boolean stareFunctionalitate, int etajMax, int etajMin, String numeLift) {
        super(pozitieEtaj, inMiscare, stareFunctionalitate, etajMax, etajMin, numeLift);
    }

    public void CheamaLift(int etajDorit, int etajApel) throws InterruptedException {
        MiscaLift(etajApel);
        System.out.println("Se deschid usile!");
        System.out.println("Se inchid usile!");
        MiscaLift(etajDorit);
    }

    private void MiscaLift(int etajDorit) throws InterruptedException {
        if (super.getPozitieEtaj() == etajDorit) {
            System.out.println("Ati apasat butonul pt etajul curent! Va rugam apasati alt buton");
        } else if (etajDorit >= super.getEtajMin() && etajDorit <= super.getEtajMax()) {
            //Liftul urca daca dorim sa mergem la un etaj superior fata de etajul curent
            if (etajDorit > super.getPozitieEtaj()) {
                for (int i = super.getPozitieEtaj(); i <= etajDorit; i++) {
                    super.setInMiscare(true);
                    Thread.sleep(2000);
                    super.setPozitieEtaj(i);
                    System.out.println("Liftul " + super.getNumeLift() + ", este la etajul " + super.getPozitieEtaj());
                }
                super.setInMiscare(false);
            }

            //Liftul coboara daca dorim sa mergem la un etaj inferior fata de etajul curent
            if (etajDorit < super.getPozitieEtaj()) {
                for (int i = super.getPozitieEtaj(); i >= etajDorit; i--) {
                    super.setInMiscare(true);
                    Thread.sleep(2000);
                    super.setPozitieEtaj(i);
                    System.out.println("Liftul " + super.getNumeLift() + ", este la etajul " + super.getPozitieEtaj());
                }
                super.setInMiscare(false);
            }
        }
    }
}
