public class Main {
    public static void main(String[] args) throws Exception {
        int etajApel;
        int etajDorit;

        Lift liftA = new Lift(0, false, false, 10, -2, "A");
        Lift liftB = new Lift(0, false, true, 10, -2, "B");
        while (true) {
            etajApel = PanouComanda.SelecteazaEtajCurent();
            etajDorit = PanouComanda.SelecteazaEtajDorit();

            Lift liftGasit = PanouComanda.GasesteLift(liftA, liftB);
            liftGasit.CheamaLift(etajDorit, etajApel);
        }
    }
}
