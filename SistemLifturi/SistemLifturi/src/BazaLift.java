class BazaLift {
    private int pozitieEtaj;
    private boolean inMiscare;
    private boolean stareFunctionalitate;
    private int etajMax;
    private int etajMin;
    private String numeLift;

    protected BazaLift(int pozitieEtaj, boolean inMiscare, boolean stareFunctionalitate, int etajMax, int etajMin, String numeLift) {
        this.pozitieEtaj = pozitieEtaj;
        this.inMiscare = inMiscare;
        this.stareFunctionalitate = stareFunctionalitate;
        this.etajMax = etajMax;
        this.etajMin = etajMin;
        this.numeLift = numeLift;
    }

    protected int getPozitieEtaj() {
        return pozitieEtaj;
    }

    protected boolean getInMiscare() {
        return inMiscare;
    }

    protected boolean getStareFunctionalitate() {
        return stareFunctionalitate;
    }

    protected int getEtajMax() {
        return etajMax;
    }

    protected int getEtajMin() {
        return etajMin;
    }

    protected String getNumeLift() {
        return numeLift;
    }

    protected void setPozitieEtaj(int pozitieEtaj) {
        this.pozitieEtaj = pozitieEtaj;
    }

    protected void setInMiscare(boolean inMiscare) {
        this.inMiscare = inMiscare;
    }
}
