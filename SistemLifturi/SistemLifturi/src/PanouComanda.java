import java.util.ArrayList;
import java.util.Scanner;

public class PanouComanda {
    public static Lift GasesteLift(Lift liftA, Lift liftB) throws Exception {
        ArrayList<String> list=new ArrayList<String>();
        list.add(liftA.getNumeLift());
        list.add(liftB.getNumeLift());

        System.out.println("Lifturile disponibile in cladire:");
        for (String s:list) {
            System.out.println(s);
        }

        if (liftA.getStareFunctionalitate()) {
            System.out.println("Liftul A a fost selectat");
            return liftA;
        } else if (liftB.getStareFunctionalitate()) {
            System.out.println("Liftul B a fost selectat");
            return liftB;
        } else {
            throw new Exception("Toate Lifturile sunt dezactivate");
        }
    }

    public static int SelecteazaEtajCurent() throws Exception {
        int etajApel;
        Scanner scan = new Scanner(System.in);
        System.out.println("Introduceti etajul la care va aflati [-2 - 10]:");
        etajApel = scan.nextInt();
        if (!(etajApel >= -2) && !(etajApel <= 10)) {
            throw new Exception("Ati introdus un etaj inexistent");
        }
        return etajApel;
    }

    public static int SelecteazaEtajDorit() throws Exception {
        int etajDorit;
        Scanner scan = new Scanner(System.in);
        System.out.println("Introduceti etajul la care doriti sa ajungeti [-2 - 10]:");
        etajDorit=scan.nextInt();
        if(!(etajDorit>=-2) && !(etajDorit<=10)) {
            throw new Exception("Ati introdus un etaj inexistent");
        }
        return etajDorit;
    }
}
