package curs.ssatr.chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientHandler extends Thread {

    private PrintWriter sout;
    private BufferedReader sin;
    private ChatServer srv;
    private boolean active = true;
    String myName;

    ClientHandler(Socket socket, ChatServer srv) throws IOException {
        this.srv = srv;
        sout
                = new PrintWriter(
                        new OutputStreamWriter(
                                socket.getOutputStream()), true);

        sin
                = new BufferedReader(
                        new InputStreamReader(
                                socket.getInputStream()));
    }

    public void run() {
        try {
            sout.println("Wellcome to SSATR chat server!");
            sout.println("What is your name?");
            String rcv = sin.readLine();
            myName = rcv;
            sout.println("Hi " + rcv);

            while (true) {
                rcv = sin.readLine();
                if (rcv.contains(":")) {
                    String privateName = rcv.substring(0, rcv.indexOf(":"));
                    String message = rcv.substring(rcv.indexOf(":") + 1, rcv.length());
                    srv.sendToOne(message, privateName);
                    System.out.println("send to one");
                }else{
                srv.sendToAllButMe(myName + " : " + rcv, this);
                    System.out.println("send to all");
                }
            }
            //sout.println("Bye!");
        } catch (Exception e) {
            e.printStackTrace();
            active = false;
        }
    }

    void sendMessage(String msg) {
        sout.println(msg);
    }

    boolean isActive() {
        return active;
    }

}
