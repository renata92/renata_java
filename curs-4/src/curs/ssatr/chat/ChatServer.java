/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package curs.ssatr.chat;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ChatServer extends Thread{
    ArrayList<ClientHandler> clients = 
            new ArrayList<>();
    
    public void run(){
        try {
            ServerSocket ss = new ServerSocket(7878);
            while(true){               
                System.out.println("Waiting for clients...");
                Socket socket = ss.accept();
                System.out.println("Client connected!");
                ClientHandler h = new ClientHandler(socket, this);
                h.start();
                clients.add(h);
            }
        } catch (Exception ex) {
            Logger.getLogger(ChatServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    void sendToAll(String msg){
//        for(ClientHandler c: clients){
//            if(c.isActive())
//                c.sendMessage(msg);
//        
//        }
        
        Iterator<ClientHandler> iter = clients.iterator();
        while(iter.hasNext()){
            ClientHandler c = iter.next();
            if(!c.isActive())
                iter.remove();
            else
                c.sendMessage(msg);
                
        }
    }
    void sendToOne(String msg, String name){
        
        for (int i = 0; i < clients.size(); i++) {
            if (clients.get(i).myName.equals(name)){
                clients.get(i).sendMessage(msg);
            }
            else{
                System.out.println("Client " +name+ " not found");
            }
                
        }
    }
    void sendToAllButMe(String msg, ClientHandler ch){
        Iterator<ClientHandler> iter = clients.iterator();
        while(iter.hasNext()){
            ClientHandler c = iter.next();
            if(!c.isActive())
                iter.remove();
            else
            {
                if (!c.equals(ch)){
                    c.sendMessage(msg);
                }
               
            }
                
                
        }
    }
    
     public static void main(String[] args) {
        ChatServer srv = new ChatServer();
        srv.start();
    }
    
}


