/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package curs.ssatr.chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ChatClient extends Thread {

    private PrintWriter sout;
    private BufferedReader sin;

    public void starChat() throws IOException {
        Socket socket = new Socket("192.168.7.118", 7878);
        sout
                = new PrintWriter(
                        new OutputStreamWriter(
                                socket.getOutputStream()), true);

        sin
                = new BufferedReader(
                        new InputStreamReader(
                                socket.getInputStream()));

        System.out.println(sin.readLine());
        System.out.println(sin.readLine());

        start();

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            //System.out.print("Enter something:"); am decomentat ca sa nu apara in timplul conversatiei
            String msg = br.readLine();
            sout.println(msg);
        }

    }

    public void run() {
        try {
            while (true) {
                System.out.println(sin.readLine());
            }
        } catch (IOException ex) {
            Logger.getLogger(ChatClient.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void main(String[] args) throws IOException {
        ChatClient cc = new ChatClient();
        cc.starChat();

    }

}
